Trestle.resource(:users) do
  menu do
    group :main, priority: :first do
      item :users, icon: "fa fa-users", priority: 2
    end
  end

  # Customize the table columns shown on the index view.
  #
  table do
    column :id
    column :full_name
    column :email
    column :account
    column :sign_in_count
    column :last_sign_in_at
    column :created_at, align: :center
    actions
  end

  # Customize the form fields shown on the new/edit views.
  #
  form do |user|
    row do
      col(sm: 6) { text_field :first_name }
      col(sm: 6) { text_field :last_name }
      col(sm: 6) { text_field :email }
      col(sm: 6) { select :account_id, Account.all }
    end

    row do
      col(sm: 6) { text_field :password }
      col(sm: 6) { text_field :password_confirmation }
    end
  end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  params do |params|
    params.require(:user).permit(
      :email,
      :password,
      :password_confirmation,
      :account_id,
      :first_name,
      :last_name
    )
  end
end
