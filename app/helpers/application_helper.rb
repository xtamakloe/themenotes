module ApplicationHelper

  # def active?(controller)
  #   "<hr>".html_safe if params[:controller] == controller
  # end


  def is_active?(controller)
    params[:controller] == controller
  end

  def active?(controller)
    "active active-link" if params[:controller] == controller
  end


  def starred?(resourceship)
    " starred" if resourceship.starred?
  end


  def border_style(flag_key)
    if flag_key.present?
      " style='border-color: #{Theme::FLAG_BRD[flag_key]};' ".html_safe
    end
  end


  def bottom_border_style(flag_key)
    if flag_key.present?
      " style='border-bottom: 1px solid #{Theme::FLAG_BRD[flag_key]};' ".html_safe
    end
  end


  def top_border_style(flag_key)
    if flag_key.present?
      " style='border-top: 1px solid #{Theme::FLAG_BRD[flag_key]};' ".html_safe
    end
  end


  def left_border_style(flag_key)
    if flag_key.present?
      " style='border-left:1px solid #{flag_key}'".html_safe
    end
  end


  def background_style(flag_key)
    if flag_key.present?
      " style='background: #{Theme::FLAG_BGD[flag_key]};'".html_safe
    end
  end


  def background_and_border_style(flag_key)
    if flag_key.present?
      # " style='background: #{Theme::FLAG_BGD[flag_key]};border: 1px solid #{Theme::FLAG_BRD[flag_key]};'".html_safe
      " style='background: #{Theme::FLAG_BGD[flag_key]};border-color:#{Theme::FLAG_BRD[flag_key]};'".html_safe
    end
  end


  def link_representation(title, tooltip, link_id=nil, link_klass=nil, glyph=nil)
    if glyph.present?
      "<span title='#{tooltip}' data-glyph='#{glyph}' "\
        "data-toggle='tooltip' aria-hidden='true' "\
        "class='oi theme-icon-oiconic #{link_klass}' "\
        "id='#{link_id}' "\
      "></span>".html_safe
    else
      # title
      "<span title='#{tooltip}' data-toggle='tooltip' aria-hidden='true'>#{title}</span>".html_safe
    end
  end


  def get_alert(flash)
    if flash[:success].present?
      ["success", flash[:success]]
    elsif flash[:error].present?
      ["danger", flash[:error]]
    elsif flash[:alert].present?
      ["primary", flash[:alert]]
    elsif flash[:notice].present?
      ["info", flash[:notice]]
    else
      ["", ""]
    end
  end


  def oiconic_icon(icon, klass=nil)
    "<span class='oi theme-icon-oiconic #{klass}' "\
    "data-glyph='#{icon}' aria-hidden='true'></span>".html_safe
  end


  def ionic_icon(icon, title = nil, klass = nil)
    icon_str =
      "<i class='ion-md-#{icon} theme-icon-ionic #{klass}'"\
      "data-toggle='tooltip' aria-hidden='true'"
    icon_str << "title='#{title}'" if title
    icon_str << "></i>"
    icon_str.html_safe
  end


  def add_resource_links(text, account)
    str   = text.to_s
    codes = text.scan(/@(\w+)/).flatten.uniq

    if codes.present?
      codes.each do |code|
        code_regex = "@#{code}"

        res = account.resources.where(code: code).first
        if res
          link = "<a "\
                 "href='/resources/#{res.id}' "\
                 "data-tippy-content='#{res.cite}' "\
                 "class='jqueryModal' "\
                 ">#{res.code}</a>"

        else
          link = "<span "\
                 "data-tippy-content='Resource not found' "\
                 "style='color:red;' "\
                 ">#{code_regex}</span>"
        end
        str.gsub!(code_regex, link).to_s.html_safe
      end
      str
    else
      str
    end
    # text.to_s.gsub(/@(\w+)/, "<a target='external' data-tippy-content='test' href='/resources/c/\\1'>\\1</a>").html_safe
  end


  def show_formatted_content(notes, account)
    # simple_format(add_resource_links(notes))
    add_resource_links(notes, account).html_safe
  end


  def on_page(controller, action='index')
    params[:controller] == "app/#{controller}" && params[:action] == action
  end


  def notes_present?(notes)
    empty_note = "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>"
    content    = notes.to_s
    if content.length == 0
      content.present?
    else
      !content.match?(empty_note)
    end
  end


  def pagination_details(collection)
    per_page     = collection.per_page
    current_page = collection.current_page
    prev_page    = collection.previous_page
    last_page    = collection.total_pages
    total        = collection.count
    size         = collection.size
    type         = collection.first.class.to_s.downcase.pluralize(size)

    if size > 1
      if size == total
        "Displaying <b>all #{total}</b> #{type}".html_safe
      else
        if current_page == last_page
          from = (prev_page * per_page) + 1
          to   = total
        else
          from = (current_page * per_page) - (per_page - 1)
          to   = current_page * per_page
        end
        # "Displaying #{type} <b>#{from} - #{to}</b> "\
        # "of <b>#{total}</b> in total".html_safe
        # "Displaying <b>#{from} - #{to}</b> "\
        # "of <b>#{total}</b> #{type} in total".html_safe
        "Displaying <b>#{to}</b> "\
        "of <b>#{total}</b> #{type}".html_safe
      end
    end
  end


=begin
  def project_links(theme)
    links = []
    theme.projects.each do |project|
      links << link_to(project.title, project_themes_tid_path(project, tid: theme.id))
    end
    links.join(', ').html_safe
  end
=end


  # Show resources associated with a theme in a project
  def themes_resources_by_project_links(theme)
    links = []
    theme.projects.each do |project|
      link = link_to(
        project.title.upcase,
        project_theme_resourceships_path(
          project,
          theme
        ),
        class: 'jqueryModal'
      )

      links << link
    end
    links.join(' ').html_safe
  end


  def link_to_new_resourceship_form(type, title, tooltip, klass, project, theme)
    link_to title,
            # new_forms_resourceship_path(
            new_project_theme_resourceship_path(
              project_id: project.id,
              theme_id:   theme.id,
              form_type:  type
            ),
            # 'data-toggle': 'tooltip',
            # title:         tooltip,
            'rel':         'modal:open',
            'data-tippy-content': tooltip,
            class:         "#{klass} jqueryModalLink"
  end


  def get_title(controller_params)
    title = controller_params.to_s.gsub('app/', '').to_s.titleize
    title = 'Library' if title == 'Resources'
    title = 'Notebooks' if title == 'Projects'

    title
  end



end
