// app.js: handle js for app section of site
//= require application
//= require bootstrap/js/bootstrap.bundle
//= require introjs
//= require app/tours
//= require trix
//= require rails-jquery-tokeninput
//= require popper
//= require jquery-modal/jquery.modal.min


function commentedOut() {
    //= require alertify
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}


function scrollToDiv(div) {
    setTimeout( function(){
        $('html, body').animate({
            scrollTop: div.offset().top - 100
        }, 'fast');
    }  , 800 );
}

function highlightDiv(div) {
    div.addClass("loaded");  // white => green
    setTimeout( function(){
        div.addClass("unloaded"); // green => white
    }  , 10000 );
}

function removeNote(type, id) {
    var viewer = type + '-' + id+'-note-viewer';
    var editor = type + '-' + id+'-note-editor';
    // $(element).remove();
    // $('#' +element).replaceWith("<div id='"+ element + "'></div>");
    $('#' + editor).hide();
    // $('#' + viewer).show();
}


function saveNote(type, id) {
    var form_id = 'edit_' + type + '_' + id;
    $('#' + form_id).submit();
}

function hideNotice() {
    // close all alerts after 3 s
    setTimeout(function () {
        $('.alert').alert('close'); // close all alerts after 3 secs
    }, 2000);
}

function enableAjaxPagination() {
    $(".apple_pagination").on('click', 'a', function (e) {
        e.preventDefault();
        $(".paginator").html("<p style='text-align:center'>Loading. Please wait...</p>");
        $.getScript(this.href);
        return false;
    });
}

function reInitAjaxPagination() {
    $('.apple_pagination').off();

    enableAjaxPagination();
}

function hideTooltips() {
    $('.tooltip').remove();
}

// function setupBootstrapModal() {
//     // set up modals
//     $('#modal-remote')
//         .on('show.bs.modal', function (e) {
//             $(this).find('.modal-content').load(e.relatedTarget.href);
//         })
//         .on('shown.bs.modal', function (e) {
//             setTimeout(function () {
//                 $('.selector').select2({
//                     theme: "bootstrap",
//                     dropdownParent: $('#modal-remote')
//                 });
//             }, 100);
//         })
//         .on('hidden.bs.modal', function () {
//             $('#modal-remote').removeData('bs.modal');
//         });
//
//     // forms
//     $('body').on('click', '.modal-btn', function (e) {
//         e.preventDefault();
//
//         $(this).closest('form').submit();
//
//         $('#modal-remote').modal('hide');
//         return false;
//     });
// }

function setupTooltips() {
    // activate tooltips
    $('[data-toggle="tooltip"]').tooltip().on('click', function () {
        $(this).tooltip('hide')
    });
    // hide tooltips
    $('.tooltip').not(this).hide();
}

function setupDropDown() {
    $(".dropdown-menu a").on('click', function () {
        $(this).closest(".dropdown-menu").prev().dropdown("dispose");
    });
}

function getMessage(action) {
    return '<p id="ajax-operation-notice" style="font-size:0.8rem;" class="ml-4 mt-3">'
        + action
        + '. Please wait...</p>';
}

function setupSearchAjaxSpinner() {
    $('.form-search').on('submit', function () {
        $('.content-box').html(getMessage('Searching'));
    });
}

function setupLoadingAjaxSpinner() {
    $(".theme-title").on('click', 'a', function () {
        $(this)
            .parent().parent().parent().parent()
            .find('.theme-resources')
            .html(getMessage('Loading'));
    });
}

function setupAjaxSpinners() {
    setupLoadingAjaxSpinner();
    setupSearchAjaxSpinner();
}

function setupClickableDiv(element, link_class) {
    $(element).click(function() {
        window.location = $(this).find(link_class).attr("href");
        return false;
    });
   // setupClickableDiv('.project', '.project-link');
}

function toggleNotes(themeId) {
    // hide all closest note-viewers
    var element = '#theme-' + themeId;
    $(element).find('.note-viewer').toggle();
    var label = $(element).find('.toggle-label').text().trim();
    var newLabel = (label == 'Show notes') ? 'Hide notes' : 'Show notes';
    $(element).find('.toggle-label').html(newLabel);
}

function toggleThemeNotes(themeId) {
    var element = "#theme-" + themeId + "-note-viewer";
    $(element).toggle();
    var label = "#theme-" + themeId + "-toggle-label";
    var newLabel = ($(label).text().trim() === 'View notes') ? 'Hide notes' : 'View notes';
    $(label).html(newLabel);
}

function toggleResNotes(resourceshipId) {
    // hide all closest note-viewers
    // var element = '#resourceship-' + resourceshipId;
    // $(element).find('.note-viewer').toggle();

    var element = '#resourceship-' + resourceshipId + '-note-viewer';
    $(element).toggle();

    // var label = $(element).find('.toggle-l
    // abel').text().trim();
    // var newLabel = (label == 'Show notes') ? 'Hide notes' : 'Show notes';
    // $(element).find('.toggle-label').html(newLabel);
}

function initJqueryModal() {
    // https://github.com/kylefox/jquery-modal

    $.modal.OPEN = 'modal:open';

    $('body').on($.modal.OPEN, function () {
        $('.selector').select2();

    }).on('submit', function () {
        $.modal.close();
    });

    $('.jqueryModal').on('click', function (event) {
        event.preventDefault();
        this.blur(); // Manually remove focus from clicked link.
        $.get(this.href, function (html) {
            $(html).appendTo('body').modal();

            $('.selector').select2();

            handleAutocompleteEvent('theme');

            setupTagging();
        });

        // $('body').on('submit', function () {
        //     $.modal.close();
        // });
    });
}

// Remove jquery modal init and re-apply to all elements. Prevents being attached
// twice to non-dynamic page elements e.g. Add new theme
function reInitJqueryModal() {
    $('.jqueryModal').off();

    initJqueryModal();
}


function setupTagging() {
    $('.form-tags').tokenInput('/tags.json', {
        theme: 'facebook',
        prePopulate: $('.form-tags:visible').data('load'),
        noResultsText: "",
        searchingText: "Searching...",
        preventDuplicates: true
    });
}


function handleAutocompleteEvent(type) {
    if (type === 'theme') {

        $('.form-control').on('railsAutocomplete.select', function (event, data) {
            // close modal
            $.modal.close();

            // find and display theme
            var themeId = data.item.id;

            var url = '/themes/tid/' + themeId;

            var projectId = window.location.href.match(/\d+$/);

            if (projectId) {
                url += '?project_id=' + projectId;
            }

            $.getScript(url);
        });
    }

}

function initTrix() {

    Trix.config.textAttributes.red = {
        style: {color: "red"},
        parser: function (element) {
            return element.style.color === "red"
        },
        inheritable: true
    };

    Trix.config.textAttributes.comic = {
        style: {fontFamily: "Consolas Sans MS"},
        parser: function (element) {
            return element.style.fontFamily.match(/Comic Sans MS/)
        },
        inheritable: true
    };

    /* addEventListener("trix-change", function (event) {
         alert(event.target.valueOf())
      })*/

    addEventListener("trix-initialize", function (event) {

        // event.target.toolbarElement.querySelector('.trix-button-row').hide();

        event.target.toolbarElement.querySelector('.trix-button-row').style.display = 'none'

        /*var buttonHTML = '<button type="button" data-trix-attribute="red">RED</button>'
        buttonHTML += '<button type="button" data-trix-attribute="comic" onclick="hideToolbar();">LOL</button>'

        event.target
            .toolbarElement
            .querySelector(".trix-button-group--text-tools").insertAdjacentHTML("beforeend", buttonHTML)*/
    })
}

function toggleEditorToolbar(type, id) {
    $('#' + type + '-' + id).find('.trix-button-row').toggle();
}

function insertResourceLink(themeId, resourceId, resourceCode) {

    var editor = document
        .querySelector("#theme-" + themeId + "-note-editor")
        .querySelector('trix-editor').editor

    var linkHTML = "<a class='editor-resource-code' href='/resources/" + resourceId + "'>" + resourceCode + "</a> "

    editor.insertHTML(linkHTML)

}

function initTippy() {
    tippy('[data-tippy-content]', {
            arrow: true
        }
    );
}

function disableBackButton() {
    $(document).keydown(function (e) {
        var nodeName = e.target.nodeName.toLowerCase();

        if (e.which === 8) {
            if ((nodeName === 'input' && e.target.type === 'text') ||
                nodeName === 'textarea') {
                // do nothing
            } else {
                e.preventDefault();
            }
        }
    });
}

/*
function initAccordionIcons() {

    var sidebarSections = $(".theme-resources")

    // sidebarSections.find('.fa').addClass('fa-chevron-up orange')

    // Toggle plus minus icon on show hide of collapse element
    sidebarSections.on('show.bs.collapse', function () {
        $(this)
            .find('.fa')
            .removeClass('fa-chevron-down')
            .addClass('fa-chevron-up orange')
    }).on('hide.bs.collapse', function () {
        $(this)
            .find('.fa')
            .removeClass('fa-chevron-up orange')
            .addClass('fa-chevron-down')
    });


    var themeContents = $(".theme-contents")

    themeContents.on('show.bs.collapse', function () {

        $(this).parent()
            .find('.theme-section-title')
            .find('.fa')
            .removeClass('fa-chevron-right')
            .addClass('fa-chevron-down orange')
    }).on('hide.bs.collapse', function () {
        $(this).parent()
            .find('.theme-section-title')
            .find('.fa')
            .removeClass('fa-chevron-down')
            .addClass('fa-chevron-right')
    })
}
*/

function initJS() {

    initTrix();

    setupTooltips();

    hideNotice();

    setupAjaxSpinners();

    enableAjaxPagination();

    initJqueryModal();


    // initTippy();

    // disableBackButton();
}

function reInitJS() {

    reInitJqueryModal();

    reInitAjaxPagination();
}

// window.onbeforeunload = confirmExit;
//
// function confirmExit() {
//     return "You have attempted to leave this page.  If " +
//         "you have made any changes to the fields " +
//         "without clicking the Save button, your changes will be lost.  " +
//         "Are you sure you want to exit this page?";
// }

