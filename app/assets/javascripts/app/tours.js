function startIntroTour() {
    var intro = introJs();

    intro.setOptions({
        'showStepNumbers': false,
        steps: [
            {
                intro: "<b>Hi there! </b> <br/>This tour will get you"
                + " up and running in no time by giving you a quick " +
                "overview of the main features of ResourceNote."
            },
            {
                element: document.querySelector('#projects-nav-item'),
                intro: "Notebooks help you to keep your notes organised. " +
                "You can add as many notes as you like to a notebook.",
                position: "right"
            },
            /*{
                element: document.querySelector('#projects-nav-item'),
                intro: "Notebooks help you to keep your notes organised. " +
                    "You can add as many notes as you like to a notebook, " +
                    "and only notes related to a project will be listed " +
                    "under that project",
                position: "right"
            },*/
            /*{
                element: document.querySelector('#themes-nav-item'),
                intro: "Add notes on themes, ideas, concepts " +
                    "or any topics you want to keep track of to your" +
                    " notebooks and easily access all of them from the " +
                    "master Theme list. You can also add notes, tags and different " +
                    "colours to each themes to keep track of important details.",
                position: "right"
            },*/
            {
                element: document.querySelector('#resources-nav-item'),
                intro: "The Resource Library contains the source material for your " +
                "research. These include reports, academic papers and even " +
                "web pages. Attach resources to your notes to quickly remember " +
                "where you found them. You can also add notes for each " +
                "resource you attach. The Library allows you to " +
                "manage all of this seamlessly.",
                position: "right"
            },
            {
                intro: "And that's it! Hope that was helpful. Check out the " +
                " other tours to find out how to perform specific tasks " +
                "like adding notebooks and attaching resources to notes. " +
                "<br/> You can also drop us a line via the chat bubble " +
                "if you need any help. Happy researching!"
            }
        ]
    });
    intro.start();
}

function startAddProjectTour() {
    var intro = introJs();
    intro.setOptions({
        'showStepNumbers': false,
        steps: [
            {
                intro: "Notebooks allow you to keep your themes organised."
                + " Open a project to see only themes related to that project.",
                position: "right"
            },
            {
                element: document.querySelector('#add-project'),
                intro: "Click here to add a new project. Remember to add"
                + " a short description while you're at it.\n "
                + " Once you're done, open the project and check out " +
                "the other tours to learn how you can use themes! ",
                position: "left"
            }
        ]
    }).start();
}

function startThemesTour() {
    var intro = introJs();
    intro.setOptions({
        'showStepNumbers': false,
        steps: [
            {
                intro: "The master Themes list allows you to see all your"
                + " themes from every project in one place. You can also add themes"
                + " here, but you will need to specify a project for it"
            },
            {
                element: document.querySelector('#add-new-theme'),
                intro: "Clicking here will allow you to add a theme. You "
                + " can further categorize themes by assigning colored flags to them",
                position: "left"
            },
            {
                element: document.querySelector('.theme-project'),
                intro: "The notebook list shows the notebook the note " +
                "has been added to. Click on any notebook to access the note " +
                "information for that notebook",
                position: "bottom"
            },
            {
                element: document.querySelector('.form-search'),
                intro: "Search for themes by title, keywords or notebooks in the description",
                position: "bottom"
            },
            {
                element: document.querySelector('#form-clear'),
                intro: "Clear search results using the refresh feature",
                position: "left"
            }
        ]
    }).start();
}

function startResourcesTour() {
    var intro = introJs();
    intro.setOptions({
        'showStepNumbers': false,
        steps: [
            {
                intro: "The Resources library shows all resources you available" +
                " to be linked to themes. You can add resources here or " +
                "when you're linking them to themes"
            },
            {
                element: document.querySelector('#add-resource'),
                intro: "Click here to add new resources by either entering"
                + " their details individually or adding a BibTeX citation."
                + " You can also upload a file containing BibTeX citations"
                + " to add multiple resources at once",
                position: "left"
            },
            {
                element: document.querySelector('.resource-code'),
                intro: "Resource IDs allow you to refer to resources quickly in " +
                "notes. Tack on an @ to jump to resource from notes " +
                "e.g. @smith2019multitasking",
                position: "right"
            },
            {
                element: document.querySelector('.resource-actions'),
                intro: "You can edit resources, add notes to them, view their " +
                "citation or remove them entirely",
                position: "left"
            },
            {
                element: document.querySelector('.form-search'),
                intro: "Search for resources by their name, title or authors",
                position: "bottom"
            },
            {
                element: document.querySelector('#form-clear'),
                intro: "Clear search results using the refresh feature",
                position: "left"
            },
        ]
    }).start();
    // intro.start().oncomplete(function () {
    //     window.location.href = '/?multipage=true';
    // });
}

function startAddThemes2ProjectTour() {
    var intro = introJs();
    intro.setOptions({
        'showStepNumbers': false,
        steps: [
            {
                intro: "Adding themes to a project allows you to organise your " +
                "themes better. The same theme cannot be added to another " +
                "project."
            },
            {
                element: document.querySelector('#add-new-theme'),
                intro: "Click here to  add a theme. You can further " +
                "categorize themes by assigning colored flags to them",
                position: "left"
            },
            {
                element: document.querySelector('.link-resource'),
                intro: "Once a theme is added, you can add relevant resources to it",
                position: "left"
            },
            {
                element: document.querySelector('.edit-theme-notes'),
                intro: "You can also add short notes",
                position: "left"
            },
            {
                element: document.querySelector('.view-theme-notes'),
                intro: "All resources and notes can be compiled into a single " +
                "report for easy viewing",
                position: "left"
            }
        ]
    }).start();
}

function startLinkResource2ThemeTour() {

    $('.theme-title a').first().click();

    setTimeout(function () {
        var intro = introJs();
        intro.setOptions({
            'showStepNumbers': false,
            steps: [
                {
                    intro: "Attaching resources to notes allows you to keep " +
                    "make notes on how they're relevant to the theme."
                },
                {
                    element: document.querySelector('.link-resource'),
                    intro: "Clicking the Attach button allows you to create a " +
                    "new resource or select an existing one from the Resource library",
                    position: "left"
                },
                {
                    element: document.querySelector('.edit-resource-notes'),
                    intro: "Once a resource is attached, you can add notes to it",
                    position: "left"
                },
                {
                    element: document.querySelector('.unlink-resource'),
                    intro: "Unlinking a resource removes it from the " +
                    "note, deleting any data saved. The resource remains " +
                    "in the library however, and can be re-attached to any note.",
                    position: "left"
                },
                {
                    element: document.querySelector('.resource-jump'),
                    intro: "You can view the resource in the Resource library" +
                    " by clicking on the resource ID",
                    position: "right"
                }
            ]
        });

        intro.start();
    }, 1000);
}
