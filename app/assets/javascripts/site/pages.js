// pages.js: handle js for pages section of site
//= require application
//= require bootstrap/js/bootstrap.bundle

//= require site/js/jquery.singlePageNav.min
//= require site/js/wow.min
//= require site/js/custom
