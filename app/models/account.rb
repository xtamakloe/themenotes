class Account < ApplicationRecord
  has_many :users, dependent: :destroy
  has_many :themes, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :resources, dependent: :destroy
  has_many :resourceships, dependent: :destroy
  has_many :messages

  validates :name, uniqueness: true
end
