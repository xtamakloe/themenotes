class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :account # belongs to only one account, can access all projects
  # in that account. so other users in that account can access all projects
  # this is for future accounts feature.


  def full_name
    [self.first_name, self.last_name].join(' ')
  end
end
