class Resource < ApplicationRecord
  default_scope {order('code ASC')}

  attr_accessor :auto_fill, :citation_format, :resource_id

  after_initialize :init_default_attrs
  # before_validation :auto_fill_from_citation
  before_save :format_code


  belongs_to :account

  has_many :resourceships, dependent: :destroy
  has_many :themes, through: :resourceships
  has_many :projects, through: :resourceships

  has_many :taggings, as: :taggable

  validates :title, :code, presence: true # title & code required
  validates :code, uniqueness: { scope: :account_id, message: "already used" }
  validates :code, format: { without: /\s/ } # don't allow whitespace


  def label
    lbl = ""
    lbl << "#{self.code} - " if self.code.present?
    lbl << self.title.to_s
    lbl.truncate(80)
  end


  def resourceship_for_project_theme(project, theme)
    self.resourceships.where(project: project, theme: theme).first
  end


  def exportable_notes_for_project_theme(project, theme)
    rs = resourceship_for_project_theme(project, theme)

    ReverseMarkdown.convert(rs.notes) if rs
  end


  def cite
    c = "#{self.title}"
    unless web_resource?
      if self.authors.present?
        c << " <br/> #{self.authors}"
        if self.year_published.present?
          c << " (#{self.year_published})"
        end
      end
    end

    c.html_safe
  end


  def authors_year
    a = self.authors
    if self.year_published.present?
      a << " (#{self.year_published})"
    end

    a.html_safe
  end

  def web_resource?
    self.link.present?
  end


  # this should do for now, until i figure out how to
  # differentiate between web pages/links and reports/papers
  def academic?
    self.authors.present? && self.year_published.present?
  end


  def pretty_code
    "@#{self.code}"
  end


  def format_code
    self.code = code.to_s.gsub(/\s/, '') if code
  end


  # def auto_fill_from_citation
  #   if auto_fill == "1" && self.citation.present?
  #
  #     Resource.import(self.account, self.citation, self.code)
  #
  #     # info = Resource.parse_citation(citation_format, citation)
  #
  #     # return errors[:base] << info[:error] if info.dig(:error)
  #
  #     # self.title          = info[:title]
  #     # self.authors        = info[:authors]
  #     # self.year_published = info[:year]
  #     # self.abstract       = info[:abstract]
  #
  #     # self.keywords       = info[:keywords]
  #
  #     # self.citation       = citation.gsub(';', '') # remove ; characters
  #   end
  # end


  # Formats: string | bib-string | bib-file
  def self.parse_citation(format, citation)
    if format == 'bib-string'
      # Parse bib entry
      parsed_data = parse_bib_data(citation)
      bib_data    = parsed_data[:bib_data]
      bib         = bib_data.first
      return { error: "Invalid BibTeX citation entry supplied." } unless bib && bib.instance_of?(BibTeX::Entry)

      # Extract fields
      title    = bib.title
      authors  = bib.authors
      year     = bib.year
      abstract = bib.try(:abstract) || ''

    else
      # Parse string
      split = citation.split(';')
      return { error: "Invalid citation format." } unless split.length == 4

      # Extract fields
      authors  = split[0]
      year     = split[1].tr('().', '').strip.to_s.presence || citation.scan(/\b\d{4}\b/).first
      title    = split[2].strip
      abstract = ''
    end

    { authors: authors, year: year, title: title, abstract: abstract }
  end


  # TODO: 3. finally, this work should be done in a bg worker thread
  # Issues handled are:
  # - bad file format i.e. not bib file
  # - code already exists
  # - code not provided
  # NB: file should be File.open(file)'ed
  # Takes a bib file/string and parses to return bib data
  def self.import(users_account, file_or_string, code = nil)
    # Parse file
    parsed_data = parse_bib_data(file_or_string)

    bib_data       = parsed_data[:bib_data]
    error_messages = parsed_data[:error_messages]

    # Proceed only if no errors
    if error_messages.empty?
      results =
        if code
          process_data(users_account, bib_data, code) # TODO: Potential bug if more than 1 bib entry
        else
          process_data(users_account, bib_data)
        end
    end

    {
      found_count:    bib_data.count,
      successful:     results.try(:[], :successful_entries).presence || [],
      failed:         results.try(:[], :failed_entries).presence || [],
      error_messages: results.try(:[], :error_messages).presence || error_messages.presence
    }
  end


  # TODO: hold on separating data and file methods for now
  # To import a single reference from string data
  # def self.import_data(users_account, code, data); end


  def self.process_data(users_account, bib_data, code = nil)
    successful_entries = []
    failed_entries     = []
    error_messages     = []

    # Create resources
    bib_data.each_with_index do |bib, index|
      if bib.instance_of?(BibTeX::Error) # Detect errors in individual entries
        key = "Entry #{index + 1}"

        failed_entries << key

        error_messages << "Error in #{key}: #{bib.trace.last}"
      else
        key      = code || bib.key
        resource = users_account.resources.new(
          code:           key,
          title:          bib.title,
          authors:        bib.authors,
          year_published: bib.year,
          abstract:       bib.try(:abstract) || '',
          keywords:       bib.try(:keywords) || '',
          citation:       bib.to_s
        )
        if resource.save
          # create taggings here
          create_tags(resource, bib)

          successful_entries << resource.id # key => using id instead of code as key for now
        else
          failed_entries << key

          error_messages << "Error in entry '#{key}': "\
                          "#{resource.errors.full_messages.first}."
        end
      end
    end
    {
      successful_entries: successful_entries,
      failed_entries:     failed_entries,
      error_messages:     error_messages
    }
  end


  def tags
    taggings.includes(:tag).map(&:tag)
  end


  private


  def self.parse_bib_data(input)
    error_messages = []
    bib_data       = []

    # Parse file/string
    begin
      bib_data =
        if input.class == String
          BibTeX.parse(input)
        else
          BibTeX.open(input.path)
        end

    rescue BibTeX::ParseError => e # malformed bib
      begin
        entry = e.message.scan(/^.*?data\=\[[^\d]*(\d+)[^\d]*\].*$/)
          .first.first.to_i + 1
      rescue
        entry = 1
      end

      error_messages << "Failed to process the file due to "\
                        " an error in Entry #{entry}. Please check that"\
                        " the entry if properly formatted."
    end

    if error_messages.empty?
      if bib_data.count == 0
        error_messages << "No bib entries found! Please check for valid"\
                      " entries."
      end
    end

    {
      bib_data:       bib_data,
      error_messages: error_messages
    }
  end


  # creates tags from bib data
  def self.create_tags(resource, bib)
    if bib.try(:keywords).present?

      keywords_str = bib.keywords.to_s

      keywords = keywords_str.match(';') ? keywords_str.split(';') : keywords_str.split(',')

      # keywords = bib.keywords.to_s.split(',')

      if keywords.present? && keywords.count > 0
        keywords.each do |keyword|
          name = keyword.strip
          tag  = Tag.find_by_name(name)
          tag  = Tag.create(name: name) unless tag

          Tag.assign('Resource', resource.id, tag.id)
        end
      end
    end
  end


  def init_default_attrs
    @auto_fill       ||= "0"
    @citation_format ||= "string"
  end


end


=begin
  def self.original_parse_citation(citation)

    split = citation.split(';')

    return { error: "Invalid citation import format" } unless split.length == 4

    {
      authors: split[0],
      year:    split[1].tr('().', '').strip.to_s.presence ||
                 citation.scan(/\b\d{4}\b/).first,
      title:   split[2].strip,
    }
  end


  def original_auto_fill_from_citation
    if auto_fill == "1" && self.citation.present?
      info = Resource.original_parse_citation(citation)

      return errors[:base] << info[:error] if info.dig(:error)

      self.title          = info[:title]
      self.authors        = info[:authors]
      self.year_published = info[:year]

      self.citation = citation.gsub(';', '') # remove ; characters
    end
  end


  def self.original_import(users_account, file)
    error_messages     = []
    successful_entries = []
    failed_entries     = []

    # Parse file
    begin
      bib_data =
        if file.class == String
          BibTeX.parse(file)
        else
          BibTeX.open(file.path)
        end

    rescue BibTeX::ParseError => e # malformed bib
      entry = e.message.scan(/^.*?data\=\[[^\d]*(\d+)[^\d]*\].*$/)
        .first.first.to_i + 1

      error_messages << "Failed to process due to "\
                        " an error in Entry #{entry}. Please check that"\
                        " the entry if properly formatted."
      return { error_messages: error_messages }
    end

    if bib_data.count == 0
      error_messages << "No bib entries found! Please check for valid"\
                      " entries."
    end

    # Create resources
    bib_data.each do |bib|
      key      = bib.key
      resource = users_account.resources.new(
        code:           key,
        title:          bib.title,
        authors:        bib.authors,
        year_published: bib.year,
        citation:       bib.to_s
      )
      if resource.save
        successful_entries << key
      else
        failed_entries << key
        error_messages << "Error on entry '#{key}': "\
                          "#{resource.errors.full_messages.first}."
      end
    end

    {
      found_count:    bib_data.count,
      successful:     successful_entries,
      failed:         failed_entries,
      error_messages: error_messages
    }
  end


# def self.parse_citation(citation)
#   # anything between ').' and '.'
#   # title = citation.scan(/\)\.([^\.]*)/).flatten.first.try(:strip)
#   ## after first digit till period(.), remove preceding period(.)
#   title = citation.scan(/\)\.([^\.]*)/).flatten.first.try(:strip)
#   # beginning of sentence to (\d
#   # authors = citation.scan(/^.+?(?=\(\d)/).first.try(:strip)
#   ## beginning of sentence till first digit
#   authors = citation.scan(/^.+?(?=\d)/).first.gsub("(", "").try(:strip)
#   # first match for any digit b/n '(' and ')'
#   # year = citation.scan(/^.*?\([^\d]*(\d+)[^\d]*\).*$/).last.try(:first)
#   ## first occurence of 4 digits in string
#   year = citation.scan(/\b\d{4}\b/).first
#   { title: title, authors: authors, year: year }
# end
=end
