# Can only belong to one project. If a theme needs to be re-used in a different
# project, a copy should be made and assigned to the project. This is because
# the theme's notes are stored in this model. If added to another project,
# notes are not necessarily the same. If notes are moved out of this model, then
# maybe can consider a theme belonging to multiple projects
# Update 10/10/2018: so i've moved the notes out of the project, so the theme
# can be added to multiple projects and its notes will be different
class Theme < ApplicationRecord
  default_scope {order('title ASC')}

  belongs_to :account

  has_many :project_theme_assignments, dependent: :destroy
  has_many :projects, through: :project_theme_assignments

  has_many :resourceships, dependent: :destroy
  has_many :resources, through: :resourceships

  has_many :sub_notes, dependent: :destroy

  has_many :taggings, as: :taggable

  validates :title, :account_id, presence: true

  after_save :assign_tags


  FLAGS = {
    red:    '#FC3D39',
    orange: '#F58F25',
    yellow: '#F1C104',
    green:  '#4DBD04',
    blue:   '#26A8EE',
    purple: '#C46FD8',
    gray:   '#8E8E91',
    default: '#FFFFFF',
  }

  FLAG_BGD = {
    red:    '#FFE6EA',
    orange: '#FFF2E6',
    yellow: '#FFFFE6',
    green:  '#E6FFEA', #'#E6FFEA',
    blue:    '#E6F2FF',
    purple:  '#FFE6FB',
    gray:    '#f8f9fa',
    default: '#FFFFFF',
  }

  FLAG_BRD = {
    red:     '#FFCCCC',
    orange:  '#FFD9B3',
    yellow:  '#FFF2B3',
    green:   '#ACE1AF',
    blue:    '#C8E1FF',
    purple:  '#FFCCEE',
    gray:    '#E1E4E8',
    default: '#E0E7ED' #E2ECFF',
  }


  def flag_key
    flag.present? ? self.flag.to_s.to_sym : :default
  end


  # def descriptive_title
  #   self.title + " (#{self.project.title})"
  # end

  # Resources in the account that have not already been
  # assigned to this theme
  def available_resources
    self.account.resources
  end


  # TODO: added cos of habtm change. remove when all instances of theme.project are updated
  def project
    self.projects.first
  end


  def notes_for_project(project)
    self.project_theme_assignments.where(project_id: project.id).first.try(:notes)
  end

  def exportable_notes_for_project(project)
    notes = self.project_theme_assignments
      .where(project_id: project.id).first
      .try(:notes)

    ReverseMarkdown.convert notes if notes.present?
  end


  def resources_for_project(project)
    self.resourceships
      .order('created_at desc')
      .includes(:resource)
      .where(project_id: project.id)
      .map(&:resource)
  end


  # NB: Never use theme.resources because that returns duplicate resources
  # since the same resource can be assigned to the same theme in different
  # projects.
  # TODO: To get unique set of resources for a theme, fill out this method
  # def resources
  # end
  # will be useful when themes can be shared between accounts
  # alias_method :resources, :resources_for_account


  attr_reader :tag_tokens

  attr_accessor :tag_ids

  def tag_tokens=(tokens)
    self.tag_ids = Tag.ids_from_tokens(tokens)
  end


  def tags
    taggings.includes(:tag).map(&:tag)
  end


  def resource_tags
    self.resources.map(&:tags).flatten.uniq
  end


  def assign_tags
    # clear all old tags
    self.taggings.each {|tagging| tagging.destroy}

    reload

    # create new ones
    self.tag_ids.each do |tag_id|
      # Tagging.create(tag_id: tag_id, taggable_id: self.id, taggable_type: 'Theme')
      Tag.assign('Theme', self.id, tag_id)
    end
  end
end
