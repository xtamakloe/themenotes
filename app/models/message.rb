class Message < ApplicationRecord
  belongs_to :account

  validates :name, :email, :body, presence: true
end
