class Project < ApplicationRecord
  default_scope {order('created_at ASC')}

  belongs_to :account # projects belong to an account, but i need a flag
  # to set owns the project. to be used for multiple users in an account feature

  has_many :project_theme_assignments, dependent: :destroy
  has_many :themes, through: :project_theme_assignments

  has_many :resourceships, dependent: :destroy
  has_many :resources, through: :resourceships

  validates :description, length: { maximum: 250 }
  validates :title, presence: true

  scope :pinned, -> {where(pinned: true)}
  scope :unpinned, -> {where(pinned: false)}
end
