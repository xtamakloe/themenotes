class Resourceship < ApplicationRecord
  belongs_to :theme
  belongs_to :resource
  belongs_to :account
  belongs_to :project

  validates :theme_id, :resource_id, :project_id, presence: true

  validates_uniqueness_of :project_id, scope: %i[theme_id resource_id],
                          message:  'resource is already attached'
end
