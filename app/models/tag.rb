class Tag < ApplicationRecord
  before_save :convert_to_downcase

  has_many :taggings, dependent: :destroy


  def self.tokens(query)
    tags = where("name like ?", "%#{query}%")
    if tags.empty?
      [{ id: "<<<#{query}>>>", name: "New: \"#{query}\"" }]
    else
      tags
    end
  end


  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) {create!(name: $1).id}
    tokens.split(',')
  end


  def self.assign(taggable_type, taggable_id, tag_id)
    Tagging.create(
      tag_id:        tag_id,
      taggable_id:   taggable_id,
      taggable_type: taggable_type
    )
  end


  def resources
    self.taggings.where(taggable_type: 'Resource')
  end


  def themes
    self.taggings.where(taggable_type: 'Theme')
  end


  private


  def convert_to_downcase
    self.name.to_s.downcase!
  end

end
