class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  def generate_flash_message(message, object)
    msg = message

    msg += "<ul>"
    object.errors.full_messages.each do |m|
      msg += "<li>#{m}</li>"
    end
    msg += "</ul>"
  end
end
