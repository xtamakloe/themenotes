class Site::PagesController < ApplicationController

  layout 'site'


  def alpha
  end


  def welcome
  end

  def mvp
    render layout: false
  end


  def privacy_old
  end


  def privacy

  end


  def terms
  end

  def robots
    respond_to :txt
    # expires_in 6.hours, public: true
  end
end
