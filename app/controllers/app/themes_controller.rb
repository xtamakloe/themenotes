class App::ThemesController < App::BaseController
  before_action :retrieve_theme, only: [:edit, :update, :destroy]
  before_action :retrieve_project, except: [:destroy]

  layout false

  autocomplete :theme, :title, full: true


  # 1. This index action is used for 'Load more' themes fn as well as theme
  # autocomplete fn
  # Hence, if tid is present => autocomplete, else load more
  # 2. It also handles listing both account and project themes via js
  def index
    themes        = current_account.themes
    @none_created = themes.empty?

    # TODO: add tagging support
    #       if params[:tag] Theme.tagged_with(params[:tag])

    @q     = themes.ransack(params[:q])
    res, @action =
      if params[:tid]
        [themes.where(id: params[:tid]), 'replace']
      else
        [
          @q.result(distinct: true)
          .includes(
            :project_theme_assignments,
            :projects,
            :resourceships,
            :resources,
          ),
          'append'
        ]
      end

    @account_themes = res.paginate(
      page:     params[:page],
      per_page: params[:per_page]
    )

    render :layout => "app"
  end

  def new
    @theme        = current_account.themes.new
    @project_list = current_account.projects - @theme.projects
  end


  def create
    @theme = current_account.themes.new(theme_params)
    @theme.projects << @project if @project

    if @theme.save
      flash.now[:success] = "Note added"
    else
      flash.now[:error] = generate_flash_message(
        "Unable to add note",
        @theme
      )
    end

    @type   = @project && params[:project_id] ? 'project' : 'account'
    @themes = Theme.where(id: @theme).paginate(page: params[:page])

    respond_to do |format|
      format.js
    end
  end


  def edit
    projects        = current_account.projects.order('title asc')
    @assign_project = params['opt'] == 'assign-project'
    @account_theme  = params['opt'] == 'account-theme'
    @project_list   = @assign_project ? projects - @theme.projects : projects
  end


  def update
    @assign_project = params['opt'] == 'assign-project'
    @remove_project = params['opt'] == 'remove-project'
    @account_theme  = params['opt'] == 'account-theme'

    if @assign_project
      if @project
        ProjectThemeAssignment.create(
          project_id: @project.id,
          theme_id:   @theme.id
        )
        flash.now[:success] = "Added to #{@project.title}"
      end

    elsif @remove_project

      assignment = ProjectThemeAssignment.where(
        project_id: @project.id,
        theme_id:   @theme.id
      ).first

      if assignment.destroy
        flash.now[:success] = "Note removed from Notebook successfully!"
      end
    else

      if @theme.update_attributes(theme_params)
        flash.now[:success] = "Note updated"
      else
        flash.now[:error] = generate_flash_message(
          "Unable to save changes",
          @theme
        )
      end

    end

    @notifier_object = @theme

    respond_to do |format|
      format.js
    end
  end


  def destroy
    # Note v2: modifying this so it deletes themes(notes) without checking
    # if it's linked to projects, as linking themes (notes) to projects is
    # disabled for now
    linked_projects = false # @theme.projects

    if linked_projects.present?
      flash.now[:error] = "Unable to delete this note as it is currently "\
                      "linked to "\
                      "#{linked_projects.count} "\
                      "#{'project'.pluralize(linked_projects.count)}. "\
                      "You will need to remove all links to continue. ".html_safe
                      # "Click <a href='/themes/tid/#{@theme.id}'>here</a> "\
                      # "to see linked projects".html_safe
    else
      @theme.destroy
      flash.now[:success] = "Note deleted"
    end

    respond_to do |format|
      format.js
    end
  end


  private
  def retrieve_theme
    @theme = current_account.themes.find(params[:id])
    @notifier_object = @theme
  end


  def retrieve_project
    account_projects = current_account.projects

    id       =
      if params[:project_id]
        params[:project_id]
      else
        params.try(:[], 'theme').try(:[], 'project_id')
      end

    @project = account_projects.where(id: id).first if id
  end


  def theme_params
    params.require(:theme)
      .permit(
        :title,
        :description,
        :project_id,
        :flag,
        :tag_tokens
      ).merge(account: current_account)
  end
end
