class App::Resources::NotesController < App::BaseController
  before_action :load_resource

  layout false


  def show
  end


  def edit
    respond_to do |format|
      format.js
    end
  end


  def update
    if @resource.update_attributes(resource_params)
      flash.now[:success] = "Changes saved"
    else
      flash.now[:error] = generate_flash_message(
        "Error saving changes",
        @resource
      )
    end

    respond_to do |format|
      format.js
    end
  end


  private

  def load_resource
    @resource = Resource.find(params[:resource_id])
  end


  def resource_params
    params.require(:resource).permit(
      :notes
    )
  end
end
