class App::Resourceships::NotesController < App::BaseController
  before_action :load_resourceship

  layout false


  def edit
    respond_to do |format|
      format.js
    end
  end


  def update
    if @resourceship.update_attributes(resourceship_params)
      flash.now[:success] = "Changes saved"
    else
      flash.now[:error] = generate_flash_message(
        "Error saving changes",
        @resourceship
      )
    end

    respond_to do |format|
      format.js
    end
  end


  private

  def load_resourceship
    @resourceship = Resourceship.find(params[:resourceship_id])
    @notifier_object = @resourceship
  end


  def resourceship_params
    params.require(:resourceship).permit(
      :notes
    )
  end
end
