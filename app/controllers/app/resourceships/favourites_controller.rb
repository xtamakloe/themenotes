class App::Resourceships::FavouritesController < App::BaseController
  before_action :load_resourceship


  def update
    if @resourceship.update_attributes(resourceship_params)
      flash.now[:success] = 'Updated'
    else
      flash.now[:error] = generate_flash_message(
        "Unable to complete at this time!",
        @resourceship
      )
    end

    @notifier_object = @resourceship

    respond_to do |format|
      format.js do
        @theme   = @resourceship.theme
        @project = @resourceship.project

        render file: 'app/resourceships/index.js.erb'
      end
    end
  end


  private


  def load_resourceship
    @resourceship = Resourceship.find(params[:resourceship_id])
  end


  def resourceship_params
    params.require(:resourceship).permit(
      :starred
    )
  end
end
