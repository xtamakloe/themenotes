class App::ResourcesController < App::BaseController

  before_action :retrieve_resource, only: [:edit, :update, :destroy]


  def index
    resources     = current_account.resources
    @none_created = resources.empty?

    @q  = resources.ransack(params[:q])
    res =
      if params[:code]
        resources
          .where(code: params[:code])
      elsif params[:ids]
        resources
          .where(id: params[:ids].split('_'))
      else
        @q.result(distinct: true)
      end

    @resources = res.paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html
      format.js
      format.xlsx do
        response.headers['Content-Disposition'] =
          "attachment; filename=ResourceLibrary.xlsx"
      end
    end
  end


  def show
    retrieve_resource

    render layout: false
  end


  def new
    @form_type       = params[:form_type] # string | bib-file | bib-string => file, citation, details
    @resource        = current_account.resources.new
    @remote          = params[:remote] == 'true'
    render layout: false
  end


  def create
    if params[:entry_type] == 'field'
      @resource = current_account.resources.new(resource_params)
      path      =
        if @resource.save
          flash[:success] = "Resource added"
          resources_code_path(code: @resource.try(:code))

        else
          flash[:error] = generate_flash_message(
            "Failed to add resource!",
            @resource
          )
          resources_path
        end
      redirect_to path

    elsif params[:entry_type] == 'data'
      # get code and data
      import_data(params[:resource][:data], params[:resource][:code])

    else # should handle file entries or the current bib-string + bib-file
      # handle bib form entries
      import_file(params[:resource][:file])
    end
  end


  def edit
    retrieve_project_and_theme
    @remote = params[:remote] == 'true'

    render layout: false
  end


  def update
    retrieve_project_and_theme

    if @resource.update_attributes(resource_params)
      flash.now[:success] = "Resource updated"
    else
      flash.now[:error] = generate_flash_message(
        "Failed to update resource!",
        @resource
      )
    end

    respond_to do |format|
      if @theme
        @resourceship =
          Resourceship.where(
            account_id: current_account.id,
            theme_id:   @theme.id,
            project_id: @project.id,
            resource_id: @resource.id
          ).first

        @notifier_object = @resourceship

        # list resourceships
        format.js {render file: 'app/resourceships/index.js.erb'}
      else
        format.js
      end
    end
  end


  def destroy
    linked_themes = @resource.resourceships.map(&:theme).flatten

    if linked_themes.present?
      flash.now[:error] = "Unable to delete this resource as it is currently "\
                      "attached to "\
                      "#{linked_themes.count} "\
                      "#{'note'.pluralize(linked_themes.count)}. "\
                      "You will need to unlink it to continue. "\
                      "Click on title to see linked notes".html_safe
    else
      @resource.destroy
      flash.now[:success] = "Resource deleted"
    end

    respond_to do |format|
      format.js
    end
  end


  private

  def retrieve_resource
    @resource = current_account.resources.find(params[:id])
  end


  def import_data(data, code)
    results = Resource.import(current_account, data, code)

    successful = results[:successful]
    errors     = results[:error_messages]

    path =
      if errors.present?
        flash[:error] = errors.flatten.join('<br/>')
        resources_path

      elsif successful.present?
        flash[:success] = "Resource added successfully!"
        resources_ids_path(ids: successful.join('_'))
      end

    redirect_to path
  end


  def import_file(uploaded_file)
    results = Resource.import(current_account, uploaded_file)

    found_count = results[:found_count]
    successful  = results[:successful]
    failed      = results[:failed]
    errors      = results[:error_messages]

    overview = "Entries found: #{found_count} |"\
                " Added: #{successful.count} | Failed: #{failed.count}<br/><br/>"

    success_path = resources_ids_path(ids: successful.join('_'))
    fail_path    = resources_path

    path     =
      if errors.present?
        flash[:error] = overview + errors.flatten.join('<br/>')
        successful.present? ? success_path : fail_path

      elsif successful.present?
        flash[:success] = overview + "Resources added successfully!"
        success_path
      end

    redirect_to path
  end


  def resource_params
    params.require(:resource).permit(
      :type,
      :code,
      :title,
      :year_published,
      :authors,
      :link,
      :citation,
      :auto_fill,
      :citation_format,
      :file,
      :abstract
    )
  end
end
