class App::ProjectsController < App::BaseController
  #TODO: implement decorates_assigned :project

  before_action :retrieve_project, only: [:show, :edit, :update, :destroy]


  def index
    projects  = current_account.projects #.decorate
    @q        = projects.ransack(params[:q])
    res       = @q.result(distinct: true)
    @projects = res.paginate(page: params[:page], per_page: 12)

    @none_created = projects.empty?

    respond_to do |format|
      format.html
      format.js
    end
  end


  def show
    themes = @project.themes
    @q     = themes.ransack(params[:q])

    res =
      if params[:tid]
        themes.where(id: params[:tid])
      else
        @q.result(distinct: true)
          # .includes(
          #   :taggings
          # )
      end

    @project_themes = res.paginate(
      page:     params[:page],
      per_page: params[:per_page],
    )

    @page_title = @project.title

    respond_to do |format|
      format.html
      format.js
      format.xlsx do
        filename = @project.title.to_s.split(' ').join('-')
        response.headers['Content-Disposition'] =
          "attachment; filename=#{filename}.xlsx"
      end
    end
  end


  def edit
    render layout: false
  end


  def update
    @pin = params[:pin]

    if @project.update_attributes(project_params)
      flash[:success] = "Notebook updated"
    else
      flash[:error] = generate_flash_message(
        "Unable to save notebook updates:",
        @project
      )
    end
    dest =
      if @pin
        flash[:success] = "Notebook updatedw"
        projects_path
      else
        project_path(@project)
      end
    redirect_to dest
  end


  def new
    @project = current_account.projects.new
    render layout: false
  end


  def create
    @project = current_account.projects.new(project_params)
    if @project.save
      flash[:success] = "Notebook created"
      redirect_to @project
    else
      flash[:error] = generate_flash_message(
        "Notebook creation failed",
        @project
      )
      redirect_to projects_path
    end
  end


  def destroy
    assignments = ProjectThemeAssignment.where(project_id: @project.id)
    assignments.each {|a| a.destroy}
    if @project.destroy
      flash[:success] = "Notebook removed"
    end

    redirect_to projects_path
  end


  private
  def retrieve_project
    @project = current_account.projects.find(params[:id]) #.decorate
  end


  def project_params
    params.require(:project).permit(:title, :description, :pinned)
  end
end
