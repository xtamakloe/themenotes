class App::ResourceshipsController < App::BaseController
  before_action :retrieve_project_and_theme

  layout false


  def index
    respond_to do |format|
      format.html
      format.js
    end
  end


  def show
    @resourceship = @theme.resourceships.find(params[:id])
  end


  def new

    if params['form_type'] == 'citation'
      @citation   = true
      @form_title = 'Create and attach new resource from citation'
    end

    if params['form_type'] == 'fields'
      @fields     = true
      @form_title = 'Create and attach new resource from details'
    end

    if params['form_type'] == 'existing'
      @existing   = true
      @form_title = 'Select existing resource from Resource Library'
    end

    @resource = current_account.resources.new if @fields

    # @resource = current_account.resources.new if params['state'] == 'new'

    # even though this is resourceships controller, we need to create resource
    # to be able to use in resourceship. hence resource is in new
  end


  def create
    resource_id = params[:resource][:resource_id]
    notes       = params[:notes]

    @resource =
    if resource_id.present?
      # use existing resource if selected
      current_account.resources.where(id: resource_id).first # existing
    else
      # create new resource
      create_resource(params[:entry_type], resource_params)
    end

    if @resource.persisted?
      @resourceship = Resourceship.new(
        account_id:  current_account.id,
        resource_id: @resource.id,
        theme_id:    @theme.id,
        project_id:  @project.id,
        notes:       notes
      )

      @notifier_object = @resourceship

      if @resourceship.save
        flash.now[:success] = "Resource attached"
      else
        flash.now[:error] = generate_flash_message(
          "Error adding resource",
          @resourceship
        )
      end
    else
      if @resource.errors.present?
        flash.now[:error] = generate_flash_message(
          "Error creating new resource",
          @resource
        )
      end
    end

    respond_to do |format|
      format.js
    end
  end


  def edit
    @resourceship = @theme.resourceships.find(params[:id])

    respond_to do |format|
      format.html { render layout: "app"}
      format.js
    end
  end


  def update
    @resourceship = @theme.resourceships.find(params[:id])

    @notifier_object = @resourceship

    if @resourceship.update_attributes(resourceship_params)
      flash.now[:success] = "Note updated"
    else
      flash.now[:error] = generate_flash_message(
        "Error updating notes",
        @resourceship
      )
    end

    respond_to do |format|
      format.js
    end
  end


  def destroy
    @resourceship = @theme.resourceships.find(params[:id])

    if @resourceship.destroy
      @notifier_object = @theme
      flash.now[:success] = "Resource removed"
    end

    respond_to do |format|
      format.js
    end
  end


  private
  def retrieve_project_and_theme
    @project = current_account.projects.find(params[:project_id])
    @theme   = @project.themes.find(params[:theme_id])
  end


  def resourceship_params
    params.require(:resourceship).permit(
      :notes
    )
  end


  def resource_params
    params.require(:resource).permit(
      :title,
      :authors,
      :code,
      :year_published,
      :link,
      :citation,
      :auto_fill,
      :citation_format,
      :abstract
    )
  end


  # Returns a resource object
  def create_resource(entry_type, resource_params)
    if entry_type == 'field'
      resource = current_account.resources.new(resource_params) # new resource
      resource.save
      resource

    elsif entry_type == 'data'
      code    = resource_params[:code]
      data    = resource_params[:citation]
      results = Resource.import(current_account, data, code)

      if results[:successful].present?
        Resource.find(results[:successful].first)
      else
        resource = Resource.new
        resource.errors.add(:base, results[:error_messages].first)
        resource
      end
    else
      # reserved for loading a citations file, if ever needed
    end
  end
end
