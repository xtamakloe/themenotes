class App::CitationsController < App::BaseController
  layout false

  def show
    @resource = current_account.resources.find(params[:resource_id])
  end
end
