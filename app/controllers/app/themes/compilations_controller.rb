class App::Themes::CompilationsController < App::BaseController
  def show
    @theme = Theme.find(params[:theme_id])
  end
end
