# This handles notes for themes, actual notes are stored in
# project_theme_assignment model so notes for are distinct across projects
class App::Themes::NotesController < App::BaseController
  before_action :load_variables

  layout false


  def show
    respond_to do |format|
      format.js
    end
  end


  def edit
    respond_to do |format|
      format.html { render layout: 'app'}
      format.js
    end
  end


  def update
    if @project_theme_assignment.update_attributes(object_params)
      flash.now[:success] = "Changes saved"
    else
      flash.now[:error] = generate_flash_message(
        "Error saving changes",
        @project_theme_assignment
      )
    end

    respond_to do |format|
      format.js
    end
  end


  private


  def load_variables
    @theme                    = Theme.find(params[:theme_id])
    @project                  = Project.find(params[:project_id])
    @project_theme_assignment = ProjectThemeAssignment.where(
      theme_id:   @theme.id,
      project_id: @project.id
    ).first
    @notifier_object = @theme
  end


  def object_params
    params.require(:project_theme_assignment).permit(
      :notes
    )
  end
end
