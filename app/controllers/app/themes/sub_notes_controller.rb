##
# Handles sub notes attached to a main note (theme)
class App::Themes::SubNotesController < App::BaseController

  before_action :load_theme
  before_action :load_sub_note, except: [:new, :create]
  before_action :load_project, only: [:edit, :update]

  layout false


  def new
    @sub_note = @theme.sub_notes.new
  end


  def create
    @sub_note = @theme.sub_notes.new(sub_note_params)
    @notifier_object = @sub_note
    if @sub_note.save
      flash.now[:success] = "Sub-note added"
    else
      flash.now[:error] = "Error creating note"
    end

    respond_to do |format|
      format.js
    end
  end


  def edit
  end


  def update
    @notifier_object = @sub_note
    if @sub_note.update_attributes(sub_note_params)
      flash.now[:success] = 'Changes saved'
    else
      flash.now[:error] = generate_flash_message(
        'Error saving changes',
        @sub_note
      )
    end

    respond_to do |format|
      format.js
    end
  end

  def destroy
    @notifier_object = @sub_note.theme
    if @sub_note.destroy
      flash.now[:success] = 'Sub-note deleted'
    end

    respond_to do |format|
      format.js
    end
  end


  private


  def load_project
    @project = Project.find(params[:project_id])
  end


  def load_theme
    @theme = Theme.find(params[:theme_id])
  end


  def load_sub_note
    @sub_note = @theme.sub_notes.find(params[:id])
  end


  def sub_note_params
    params.require(:sub_note).permit(
      :title, :notes
    )
  end

end


