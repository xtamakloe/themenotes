class App::BaseController < ApplicationController
  # before_filter :authenticate_user
  before_action :authenticate_user!

  helper_method :current_account

  layout 'app'


  def current_account
    @current_account ||= current_user.account
  end


  def retrieve_project_and_theme
    if params[:theme_id].present?
      @theme = current_account.themes.where(id: params[:theme_id]).first
    end
    if params[:project_id].present?
      @project = current_account.projects.where(id: params[:project_id]).first
    end
  end
end