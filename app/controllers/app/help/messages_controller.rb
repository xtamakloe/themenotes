class App::Help::MessagesController < App::BaseController
  layout false


  def new
    @message = current_account.messages.new
  end


  def create
    @message = current_account.messages.new(message_params)
    if @message.save
      flash[:success] = "Thanks for your message. We will get back to you asap :)"
    else
      flash[:error] = "Failed to send your message. Please check that "\
                      "all fields are completed. Thanks :)"
    end
    redirect_to request.referer
  end


  private
  def message_params
    params.require(:message).permit(
      :name,
      :email,
      :body
    )
  end
end
