class App::ProfilesController < App::BaseController
  def show
    @user = current_user
    @page_title = "Account Settings"
  end
end
