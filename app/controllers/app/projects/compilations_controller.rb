class App::Projects::CompilationsController < App::BaseController
  def show
    @project = Project.find(params[:project_id])
  end
end
