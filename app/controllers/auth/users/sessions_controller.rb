class Auth::Users::SessionsController < Devise::SessionsController

  layout 'sessions'


  def after_sign_in_path_for(resource)
    projects_path
  end


  def after_sign_out_path_for(resource)
    new_user_session_path
  end
end
