class Auth::Users::RegistrationsController < App::BaseController

  skip_before_action :authenticate_user!, only: [:new, :create]


  def new
    @user = User.new
    render layout: "sessions"
  end


  def create
    @user         = User.new(user_params)
    account       = Account.create(name: @user.email.split('@').first) if @user.email
    @user.account = account if account


    if @user.save
      sign_in @user

      flash[:success] = 'Welcome to ResourceNote!'

      redirect_to projects_path
    else
      account.destroy if account

      flash[:error] = "An error occurred, please try again. <br/><br/>"\
                      "If you still have problems signing up, drop us an "\
                      "<a href='mailto:apps@artoconnect.com?subject=Sign Up Issues' "\
                      "style='color:#721c24;font-weight:bold'>email</a>".html_safe

      render "new", layout: "sessions"
    end
  end


  def edit
    @user = current_user
    render layout: nil
  end


  def update
    @user = current_user
    if @user.update(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      flash[:success] = "Profile updated"
      redirect_to profile_path
    else
      flash[:error] = generate_flash_message(
        "Unable to save changes",
        @user
      )
      redirect_to profile_path
    end
  end


  private

  def user_params
    # NOTE: Using `strong_parameters` gem
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
