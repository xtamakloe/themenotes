Rails.application.routes.draw do


  namespace :app do
    namespace :projects do
      get 'compilations/show'
    end
  end

  devise_for :users,
             controllers: {
               sessions:      'auth/users/sessions',
               registrations: 'auth/users/registrations',
               # passwords:     'auth/users/passwords'
             }

  as :user do
    get '/login', to: 'auth/users/sessions#new'
  end

  scope module: 'app' do
    resources :tags, only: [:index]

    resources :resourceships  do
      # for resourceship's notes field/attribute
      resource :note, only: [:edit, :update], controller: 'resourceships/notes'
      resource :favourite, only: [:update], controller: 'resourceships/favourites'
    end

    resources :themes, only: [:index, :new, :create, :edit, :update, :destroy] do
      # :new, :create => themes without projects,
      # added :edit, :update after habtm

      # for theme's notes => stored in project_theme_assigment model
      resource :note, only: [:show, :edit, :update], controller: 'themes/notes'

      resources :sub_notes, controller: 'themes/sub_notes' # v2 format

      # for compilation of all notes in theme
      resource :note_compilation, only: [:show], controller: 'themes/compilations'

      # for autocompleting theme title
      get :autocomplete_theme_title, on: :collection
    end
    get '/themes/tid/:tid', to: 'themes#index', as: :themes_tid

    resources :resources do
      resource :citation, only: [:show]
      resource :note, only: [:show, :edit, :update], controller: 'resources/notes'
    end
    get '/resources/c/:code', to: 'resources#index', as: :resources_code
    get '/resources/i/:ids', to: 'resources#index', as: :resources_ids

    resources :projects do
      resources :themes, only: [:new, :create, :edit, :update] do
        # :new, :create => themes from projects
        resources :resourceships
      end
      # for compilation of all notes in all themes
      resource :note_compilation, only: [:show], controller: 'projects/compilations'
    end
    get 'projects/:id/tid/:tid', to: 'projects#show', as: :project_themes_tid

    resource :profile, only: [:show]

    namespace :help do
      resources :messages, only: [:new, :create]
    end

    # root to: 'projects#index'
  end

  get '/alpha', to: 'site/pages#alpha'
  get '/terms', to: 'site/pages#terms'
  get '/privacy', to: 'site/pages#privacy'
  get '/robots.txt', to: 'site/pages#robots'
  get '/welcome', to: 'site/pages#welcome'

  # root to: 'site/pages#welcome'
  root to: 'site/pages#mvp'
end
