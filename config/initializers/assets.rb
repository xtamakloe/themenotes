# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'icons')
Rails.application.config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')

Rails.application.config.assets.precompile += %w(
  app/app.scss
  app/app.js
  app/narrow-jumbotron.css
  auth/users/sessions.css
  auth/users/sessions.js
  site/pages.css
  site/pages.js
  site/welcome.css
  site/welcome.js
  site/screenshot-4.png
  site/bground.jpg
  not_found.png
  mvp/mvp.css
  site/rn-screenshot-1.png
  site/rn-screenshot-2.png
  site/rn-screenshot-3.png
)

