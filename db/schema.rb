# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20201117133045) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "citext"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "administrators", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "remember_token"
    t.datetime "remember_token_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "account_id"
    t.string "name"
    t.string "email"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "open"
    t.index ["account_id"], name: "index_messages_on_account_id"
    t.index ["status"], name: "index_messages_on_status"
  end

  create_table "project_theme_assignments", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "theme_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "notes"
    t.index ["project_id"], name: "index_project_theme_assignments_on_project_id"
    t.index ["theme_id"], name: "index_project_theme_assignments_on_theme_id"
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "account_id"
    t.citext "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.boolean "pinned", default: false
    t.index ["account_id"], name: "index_projects_on_account_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "type"
    t.string "code"
    t.citext "title"
    t.string "year_published"
    t.string "authors"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "account_id"
    t.string "citation"
    t.text "notes"
    t.string "link"
    t.text "abstract"
    t.text "keywords"
    t.index ["account_id"], name: "index_resources_on_account_id"
  end

  create_table "resourceships", force: :cascade do |t|
    t.bigint "theme_id"
    t.bigint "resource_id"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.bigint "account_id"
    t.boolean "starred", default: false
    t.index ["account_id"], name: "index_resourceships_on_account_id"
    t.index ["project_id"], name: "index_resourceships_on_project_id"
    t.index ["resource_id"], name: "index_resourceships_on_resource_id"
    t.index ["starred"], name: "index_resourceships_on_starred"
    t.index ["theme_id"], name: "index_resourceships_on_theme_id"
  end

  create_table "sub_notes", force: :cascade do |t|
    t.bigint "theme_id"
    t.string "title"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["theme_id"], name: "index_sub_notes_on_theme_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.bigint "tag_id"
    t.bigint "taggable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "taggable_type"
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "themes", force: :cascade do |t|
    t.bigint "project_id"
    t.citext "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "account_id"
    t.text "description"
    t.string "flag", default: "default"
    t.index ["account_id"], name: "index_themes_on_account_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "account_id"
    t.string "first_name"
    t.string "last_name"
    t.index ["account_id"], name: "index_users_on_account_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "messages", "accounts"
  add_foreign_key "project_theme_assignments", "projects"
  add_foreign_key "project_theme_assignments", "themes"
  add_foreign_key "projects", "accounts"
  add_foreign_key "resources", "accounts"
  add_foreign_key "resourceships", "accounts"
  add_foreign_key "resourceships", "projects"
  add_foreign_key "resourceships", "resources"
  add_foreign_key "resourceships", "themes"
  add_foreign_key "sub_notes", "themes"
  add_foreign_key "taggings", "tags"
  add_foreign_key "themes", "accounts"
  add_foreign_key "users", "accounts"
end
