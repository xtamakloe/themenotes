class AddAccountReferenceToThemes < ActiveRecord::Migration[5.1]
  def change
    add_reference :themes, :account, foreign_key: true
  end
end
