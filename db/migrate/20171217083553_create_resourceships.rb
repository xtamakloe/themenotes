class CreateResourceships < ActiveRecord::Migration[5.1]
  def change
    create_table :resourceships do |t|
      t.references :theme, foreign_key: true
      t.references :resource, foreign_key: true
      t.text :notes

      t.timestamps
    end
  end
end
