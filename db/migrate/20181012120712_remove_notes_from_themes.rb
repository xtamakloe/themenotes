class RemoveNotesFromThemes < ActiveRecord::Migration[5.1]
  def change
    remove_column :themes, :notes, :text
  end
end
