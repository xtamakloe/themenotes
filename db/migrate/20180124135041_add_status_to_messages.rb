class AddStatusToMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :messages, :status, :string, default: "open"
    add_index :messages, :status
  end
end
