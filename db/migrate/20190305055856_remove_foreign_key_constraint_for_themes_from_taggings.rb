class RemoveForeignKeyConstraintForThemesFromTaggings < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :taggings, column: :taggable_id
  end
end
