class AddKeywordsToResource < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :keywords, :text
  end
end
