class AddNotesToResources < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :notes, :text
  end
end
