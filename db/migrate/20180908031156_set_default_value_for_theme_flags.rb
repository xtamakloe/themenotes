class SetDefaultValueForThemeFlags < ActiveRecord::Migration[5.1]
  def change
    change_column_default(
      :themes,
      :flag,
      from: nil,
      to:   'default'
    )
  end
end
