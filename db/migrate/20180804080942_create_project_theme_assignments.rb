class CreateProjectThemeAssignments < ActiveRecord::Migration[5.1]

  def change
    create_table :project_theme_assignments do |t|
      t.references :project, foreign_key: true
      t.references :theme, foreign_key: true

      t.timestamps
    end

    remove_index :themes, name: "index_themes_on_project_id"
    remove_foreign_key :themes, name: "fk_rails_41cd34e034"

    Theme.all.each do |t|
      ProjectThemeAssignment.create(
        project_id: t.project_id,
        theme_id:   t.id
      )
    end
  end

end
