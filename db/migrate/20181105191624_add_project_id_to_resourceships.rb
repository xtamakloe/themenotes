class AddProjectIdToResourceships < ActiveRecord::Migration[5.1]
  def change
    add_reference :resourceships, :project, foreign_key: true
  end
end
