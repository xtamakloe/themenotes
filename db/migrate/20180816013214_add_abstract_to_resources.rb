class AddAbstractToResources < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :abstract, :text
  end
end
