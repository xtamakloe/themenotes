class AddTaggableTypeToTaggings < ActiveRecord::Migration[5.1]
  def change
    add_column :taggings, :taggable_type, :string
    add_index :taggings, :taggable_type
  end
end
