class AddPinnedToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :pinned, :boolean, default: false
  end
end
