class AddNotesToProjectThemeAssignments < ActiveRecord::Migration[5.1]
  def change
    add_column :project_theme_assignments, :notes, :text
  end
end
