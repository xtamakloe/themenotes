class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.references :account, foreign_key: true
      t.string :name
      t.string :email
      t.text :body

      t.timestamps
    end
  end
end
