class RenameThemeIdToTaggableIdForTaggings < ActiveRecord::Migration[5.1]
  def change
    rename_column :taggings, :theme_id, :taggable_id
  end
end
