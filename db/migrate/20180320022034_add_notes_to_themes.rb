class AddNotesToThemes < ActiveRecord::Migration[5.1]
  def change
    add_column :themes, :notes, :text
  end
end
