class CreateSubNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :sub_notes do |t|
      t.references :theme, foreign_key: true
      t.string :title
      t.text :notes

      t.timestamps
    end
  end
end
