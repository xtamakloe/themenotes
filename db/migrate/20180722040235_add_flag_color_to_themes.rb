class AddFlagColorToThemes < ActiveRecord::Migration[5.1]
  def change
    add_column :themes, :flag, :string
  end
end
