class AddAccountIdToResourceships < ActiveRecord::Migration[5.1]
  def change
    add_reference :resourceships, :account, foreign_key: true
  end
end
