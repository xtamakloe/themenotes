class ChangeTitleColumnTypesToCitext < ActiveRecord::Migration[5.1]


  def up
    enable_extension('citext') unless extensions.include?('citext')

    change_column :projects,  :title, :citext
    change_column :resources, :title, :citext
    change_column :themes,    :title, :citext
  end


  def down
    change_column :projects,  :title, :string
    change_column :resources, :title, :string
    change_column :themes,    :title, :string

    disable_extension('citext') if extensions.include?('citext')
  end
end
