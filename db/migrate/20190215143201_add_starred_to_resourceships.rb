class AddStarredToResourceships < ActiveRecord::Migration[5.1]
  def change
    add_column :resourceships, :starred, :boolean, default: false
    add_index :resourceships, :starred
  end
end
