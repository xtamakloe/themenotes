class AddAccountRefToResources < ActiveRecord::Migration[5.1]
  def change
    add_reference :resources, :account, foreign_key: true
  end
end
