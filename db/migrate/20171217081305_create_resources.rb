class CreateResources < ActiveRecord::Migration[5.1]
  def change
    create_table :resources do |t|
      t.string :type
      t.string :code
      t.string :title
      t.string :year_published
      t.string :authors
      t.text :description

      t.timestamps
    end
  end
end
